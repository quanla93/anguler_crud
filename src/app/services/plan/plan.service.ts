import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PlanPayload} from "../../module/plan/plan/plan.payload";

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(private http: HttpClient) { }

  postPlan(planModule: PlanPayload){
    return this.http.post<any>('http://localhost:3000/plan', planModule);
  }

  getPlan(){
    return this.http.get<any>('http://localhost:3000/plan');
  }
}
