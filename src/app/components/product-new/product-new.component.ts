import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {PlanService} from "../../services/plan/plan.service";
import {PlanPayload} from "../../module/plan/plan/plan.payload";

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.scss']
})
export class ProductNewComponent implements OnInit {

  productFrom !: FormGroup;
  planPayload : PlanPayload;
  constructor(private formBuilder: FormBuilder, private planService: PlanService)
  {
    this.planPayload = {
      tracker: '',
      description: '',
      subject: '',
      date: ''
    }
  }

  ngOnInit(): void {
    this.productFrom = new FormGroup({
      tracker: new FormControl('', Validators.required),
      subject: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
    })
  }

  addProduct() {
    this.planPayload.tracker = this.productFrom.get('tracker')?.value;
    this.planPayload.description = this.productFrom.get('description')?.value;
    this.planPayload.subject = this.productFrom.get('subject')?.value;
    this.planPayload.date = this.productFrom.get('date')?.value;

    this.planService.postPlan(this.planPayload)
      .subscribe({
        next: (res) => {
          alert("plan added successfully")
        }, error: () => {
          alert("Plan added failed")
        }
      })
  }


}
